import { useState } from "react";
import { FlexColumn, FlexRow } from "../commonStyles";
import { NAVBAR, USER_ROLES } from "../../utils/constants";
import { Link } from "react-router-dom";
import { isMobile } from "react-device-detect";
import { NavHeaderText } from "../commonText";
import MenuIcon from "@mui/icons-material/Menu";
import cookie from "react-cookies";


const Navbar = () => {
  const user_role = cookie.load("user_role");
  const [isNavOpen, setIsNavOpen] = useState<boolean>(false);
  return !isMobile ? (
    <FlexRow style={{ margin: "16px 10%", width: "100%" }}>
      <FlexRow
        style={{
          width: "100%",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
          <NavHeaderText>{NAVBAR.TITLE}</NavHeaderText>
        </Link>
        
        <FlexRow
          style={{
            justifyContent: "flex-end",
            alignItems: "center",
            gap: "32px",
          }}
        >
          {user_role === USER_ROLES.SA && (
            <Link
              to={"/tickets/admin"}
              style={{ textDecoration: "none", color: "inherit" }}
            >
              <NavHeaderText style={{ fontSize: "24px", cursor: "pointer" }}>
                Admin
              </NavHeaderText>
            </Link>
          )}
          {NAVBAR.TILES.map((tile) => (
            <div>{tile}</div>
          ))}
        </FlexRow>
      </FlexRow>
    </FlexRow>
  ) : (
    <FlexColumn style={{ padding: "16px" }}>
      <FlexRow
        style={{ alignItems: "center", gap: "8px" }}
        onClick={() => setIsNavOpen(!isNavOpen)}
      >
        <MenuIcon />
        <NavHeaderText>#OYW21</NavHeaderText>
      </FlexRow>
      {isNavOpen && (
        <FlexColumn>
          {NAVBAR.TILES.map((tile) => (
            <NavHeaderText style={{ fontSize: "24px", cursor: "pointer" }}>
              <a
                href="#events"
                onClick={() => {setIsNavOpen(false)}}
                style={{ textDecoration: "none", color: "inherit" }}
              >
                {tile}
              </a>
            </NavHeaderText>
          ))}
        </FlexColumn>
      )}
    </FlexColumn>
  );
};

export default Navbar;
