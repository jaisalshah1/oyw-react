import React, { useEffect, useState } from "react";
import { OutfitText } from "../commonText";
import "./index.css";
import image1 from "../../images/posters/1.png";
import image2 from "../../images/posters/2.png";
import image3 from "../../images/posters/3.png";


interface Props {
  day: number;
  image: string;
  name: string;
  description: string;
  price: number;
  count: number;
}

const EventCard: React.FC<Props> = ({
  name,
  image,
  price,
  description,
  count,
  day,
}) => {
  const [passCount, setPassCount] = useState<number>(count);
  function animateValue(start: number, end: number, duration: number) {
    let startTimestamp: any = null;
    const step = (timestamp: any) => {
      if (!startTimestamp) startTimestamp = timestamp;
      const progress = Math.min((timestamp - startTimestamp) / duration, 1);
      setPassCount(Math.floor(progress * (end - start) + start));
      if (progress < 1) {
        window.requestAnimationFrame(step);
      }
    };
    window.requestAnimationFrame(step);
  }

  useEffect(() => {
    animateValue(200, count, 5000);
  }, []);
  return (
    <div className="hero-container">
      <div className="main-container">
        <div className="poster-container">
          {
            day === 1 ? (
              <img src={image1} className="poster" alt="" />
            ) : (<></>)
  }{          
            day === 2 ? (
              <img src={image2} className="poster" alt="" />
            ) : (<></>)
          }{        
            day === 3 ? (
              <img src={image3} className="poster" alt="" />
            ) : (<></>)
          }
        </div>
        <div className="ticket-container">
          <div className="ticket__content">
            <h4 className="ticket__movie-title">
              <OutfitText
                style={{
                  fontSize: "16px",
                  color: "#f9a444",
                  border: "2px solid #bf8d6f",
                  width: "120px",
                  margin: "0 auto",
                }}
              >{`DAY ${day}`}</OutfitText>
            </h4>

            <h4 className="ticket__movie-title">
              <OutfitText style={{ fontSize: "16px", fontWeight: 100 }}>
                {name}
              </OutfitText>
            </h4>
            <p className="ticket__movie-slogan">
              <OutfitText style={{ fontSize: "16px", color: "#cc547e" }}>
                {`${passCount} Passes Left!`}
              </OutfitText>
            </p>
            <button className="ticket__buy-btn">View</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EventCard;
