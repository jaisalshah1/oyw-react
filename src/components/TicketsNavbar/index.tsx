import { useState } from "react";
import { FlexColumn, FlexRow } from "../commonStyles";
import { NAVBAR, USER_ROLES } from "../../utils/constants";
import { Link } from "react-router-dom";
import { isMobile } from "react-device-detect";
import { NavHeaderText } from "../commonText";
import MenuIcon from "@mui/icons-material/Menu";
import cookie from "react-cookies";


const Navbar = () => {
  const token = cookie.load("token");
  const user_role = cookie.load("user_role");

  const [isNavOpen, setIsNavOpen] = useState<boolean>(false);
  
  return !isMobile ? (
    <FlexRow
      style={{
        width: "100%",
        justifyContent: "center",
        backgroundColor: "#202020",
        color: "#d8d8d8"
      }}
    >
      <FlexRow
        style={{
          width: "100%",
          justifyContent: "space-between",
          alignItems: "center",
          flex: 0.8,
        }}
      >
        <Link
          to="/"
          style={{ textDecoration: "none", color: "inherit" }}
        >
          <NavHeaderText>{NAVBAR.TITLE}</NavHeaderText>
        </Link>
        <FlexRow
          style={{
            justifyContent: "flex-end",
            alignItems: "center",
            gap: "32px",
          }}
        >
          <Link to="/" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
              Home
            </NavHeaderText>
          </Link>
          {
            user_role !== undefined ? (
              <Link to="/tickets/" style={{ color: "inherit" }}>
              <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                  Dashboard
                </NavHeaderText>
              </Link>
            ) :(<></>)
          }

          {(user_role === USER_ROLES.RU ||
            user_role === USER_ROLES.RH ||
            user_role === USER_ROLES.SA) && (
            <Link to="/tickets/register" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Register
              </NavHeaderText>
            </Link>
          )}
          
          {(user_role === USER_ROLES.SU ||
            user_role === USER_ROLES.SH ||
            user_role === USER_ROLES.SA) && (
            <Link to="/tickets/scanner" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Scan
              </NavHeaderText>
            </Link>
          )}
          
          {user_role === USER_ROLES.SA && (
            <Link to="/tickets/admin" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Admin
              </NavHeaderText>
            </Link>
          )}

          
          {
            user_role === "SA" || user_role === "RH" ? (
            <Link to="/tickets/transactions/" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Transactions
              </NavHeaderText>
            </Link>
            ) : (<></>)
          }

          {token ? (
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}
              onClick={() => {
                cookie.remove("token");
                cookie.remove("user_role");
                window.location.href = "/";
              }}
            >
              Sign Out
            </NavHeaderText>
          ) : (
            <Link to="/tickets/signin" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
              Sign In
            </NavHeaderText>
          </Link>
          )}
        </FlexRow>
      </FlexRow>
    </FlexRow>
  ) : (
    <FlexColumn
      style={{
        padding: "16px",
        width: "100%",
        backgroundColor: "#202020",
        color: "#d8d8d8"
      }}
    >
      <FlexRow
        style={{ alignItems: "center", justifyContent:"space-between", gap: "8px" }}
        onClick={() => setIsNavOpen(!isNavOpen)}
      >
        <NavHeaderText>#OYW21</NavHeaderText>
        <MenuIcon />
      </FlexRow>
      {isNavOpen && (
        <FlexColumn>
          {/* {NAVBAR.TILES.map((tile) => (
            <NavHeaderText
              style={{ fontSize: "24px", cursor: "pointer" }}
            >
              <a
                href="#events"
                onClick={() => setIsNavOpen(false)}
                style={{ textDecoration: "none", color: "inherit" }}
              >
                {tile}
              </a>
            </NavHeaderText>
          ))} */}
          
          <Link to="/" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
              Home
            </NavHeaderText>
          </Link>
          
          {
            user_role !== undefined ? (
              <Link to="/tickets/" style={{ color: "inherit" }}>
              <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                  Dashboard
                </NavHeaderText>
              </Link>
            ) :(<></>)
          }

          {(user_role === USER_ROLES.RU ||
            user_role === USER_ROLES.RH ||
            user_role === USER_ROLES.SA) && (
            <Link to="/tickets/register" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Register
              </NavHeaderText>
            </Link>
          )}
          
          {(user_role === USER_ROLES.SU ||
            user_role === USER_ROLES.SH ||
            user_role === USER_ROLES.SA) && (
            <Link to="/tickets/scanner" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Scan
              </NavHeaderText>
            </Link>
          )}
          
          {user_role === USER_ROLES.SA && (
            <Link to="/tickets/admin" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Admin
              </NavHeaderText>
            </Link>
          )}

          {
            user_role === "SA" || user_role === "RH" ? (
            <Link to="/tickets/transactions/" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
                Transactions
              </NavHeaderText>
            </Link>
            ) : (<></>)
          }
          
          {token ? (
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}
            onClick={() => {
              cookie.remove("token");
              cookie.remove("user_role");
              window.location.href = "/";
            }}
          >
            Sign Out
          </NavHeaderText>
          ) : (
            <Link to="/tickets/signin" style={{ color: "inherit" }}>
            <NavHeaderText style={{ fontSize: "20px", cursor: "pointer", paddingBottom: "5px" }}>
              Sign In
            </NavHeaderText>
          </Link>
          )
        }
        </FlexColumn>
      )}
    </FlexColumn>
  )
};

export default Navbar;
