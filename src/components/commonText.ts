import styled from "styled-components";

export const NavHeaderText = styled.div`
  font-family: "Outfit", sans-serif;
  font-size: 32px;

  &:hover {
    transform: scale3d(1.1, 1.1, 1.1);
    transition: all 0.2s ease-in-out;
  }
`;

export const OutfitText = styled(NavHeaderText)`
  &:hover {
    transform: none;
  }
`;

export const ErrorText = styled(OutfitText)`
  font-size: 13px;
  color: red;
`;
