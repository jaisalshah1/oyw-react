import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { API_SERVER_URL } from "../utils/constants";
import type { AppDispatch, RootState } from "./configureStore";
import axios from "../utils/httpServices";
import cookie from "react-cookies";

interface Admin {
  password: string;
  role: string;
  timestamp: string;
  id: number;
  name: string;
  phone: string;
  enabled: boolean;
}

interface AdminState {
  loading: boolean;
  admins: Admin[];
  addAdminModalOpen: boolean;
}

const initialState: AdminState = {
  loading: false,
  admins: [],
  addAdminModalOpen: false,
};

export const adminSlice = createSlice({
  name: "admin",
  initialState,
  reducers: {
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    setAdmins: (state, action: PayloadAction<Admin[]>) => {
      state.admins = action.payload;
    },
    toggleAdmin: (state, action: PayloadAction<string>) => {
      let adminIndex = state.admins.findIndex(
        (admin) => admin.phone === action.payload
      );
      state.admins[adminIndex].enabled = !state.admins[adminIndex].enabled;
    },
    toggleAddAdminModalOpen: (state, action: PayloadAction<boolean>) => {
      state.addAdminModalOpen = action.payload;
    },
  },
});

export const fetchAdmins =
  () => async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/view_users/";
      let response: { [key: string]: any } = await axios.post(
        url,
        {},
        {
          headers: {
            token: token,
          },
        }
      );
      dispatch(setLoading(false));

      if (response.data.result) {
        dispatch(setAdmins(response.data.data));
      }
      //   if (response.data.data.result) {
      //     dispatch(setAdmins(response.data.data.));
      //   }
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const enableAdmin =
  (phone: string) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/enable_user/";
      let response: { [key: string]: any } = await axios.post(
        url,
        { phone },
        {
          headers: {
            token: token,
          },
        }
      );
      dispatch(setLoading(false));
      if (response.data.result) {
        dispatch(toggleAdmin(phone));
      }

      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const disableAdmin =
  (phone: string) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/disable_user/";
      let response: { [key: string]: any } = await axios.post(
        url,
        { phone },
        {
          headers: {
            token: token,
          },
        }
      );
      dispatch(setLoading(false));
      if (response.data.result) {
        dispatch(toggleAdmin(phone));
      }
      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const registerAdmin =
  (body: { name: string; phone: string; password: string; role: string }) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/register_user/";
      let response: { [key: string]: any } = await axios.post(url, body, {
        headers: {
          token: token,
        },
      });
      dispatch(setLoading(false));
      if (response.data.result) {
        dispatch(fetchAdmins());
        dispatch(toggleAddAdminModalOpen(false));
      }
      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const { setLoading, setAdmins, toggleAdmin, toggleAddAdminModalOpen } =
  adminSlice.actions;

export default adminSlice.reducer;
