import {createSlice, PayloadAction } from "@reduxjs/toolkit";
import { API_SERVER_URL, USER_ROLES } from "../utils/constants";
import type { AppDispatch, RootState } from "./configureStore";
import axios from "../utils/httpServices";
import cookie from "react-cookies";
interface Event {
  name: string;
  image: string;
  description: string;
  price: number;
}

interface PassCount {
  day1: number;
  day2: number;
  day3: number;
  redeemed_singing: number;
  redeemed_drama: number;
  redeemed_dance: number;
}

interface HomeState {
  loading: boolean;
  events: Event[];
  user: {
    role: string;
  };
  passesCounts: PassCount;
}

const initialState: HomeState = {
  loading: false,
  events: [
    {
      name: "Singing",
      image: "https://picsum.photos/200/300",
      description: "lorem ipsim lorem ipsimlorem ipsimlorem ipsimlorem ipsim",
      price: 20,
    },

    {
      name: "Drama",
      image: "https://picsum.photos/200/300",
      description: "lorem ipsim lorem ipsimlorem ipsimlorem ipsimlorem ipsim",
      price: 20,
    },
    {
      name: "Dance",
      image: "https://picsum.photos/200/300",
      description: "lorem ipsim lorem ipsimlorem ipsimlorem ipsimlorem ipsim",
      price: 20,
    },
  ],
  user: {
    role: USER_ROLES.NA,
  },
  passesCounts: {
    day1: 200,
    day2: 200,
    day3: 200,
    redeemed_singing: 0,
    redeemed_drama: 0,
    redeemed_dance: 0,
  },
};

export const homeSlice = createSlice({
  name: "home",
  initialState,
  reducers: {
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    setUserRole: (state, action: PayloadAction<string>) => {
      state.user.role = action.payload;
    },
    setPassCount: (state, action: PayloadAction<PassCount>) => {
      state.passesCounts = action.payload;
    },
  },
});

export const signIn =
  (phone: string, password: string) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      let body = { phone: phone, password: password };
      dispatch(setLoading(true));
      let url = API_SERVER_URL + "/auth/sign_in/";
      let response: { [key: string]: any } = await axios.post(url, body);
      if (response.data.result) {
        dispatch(setUserRole(response.data.data.user_role));
        // localStorage.setItem("user_role", response.data.data.role);
        cookie.save("user_role", response.data.data.user_role, {});
        cookie.save("token", response.data.token, {});
        console.log("cookie saved");
        
        window.location.href = "/tickets/";
        dispatch(setLoading(false));
      } else {
        alert("User not found");
      }
      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const getPassesCounts =
  () => async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let url = API_SERVER_URL + "/passes/counts/";
      let response: { [key: string]: any } = await axios.get(url);
      if (response.data.result) {
        const {
          singing,
          drama,
          dance,
          redeemed_dance,
          redeemed_singing,
          redeemed_drama,
        } = response.data.data;
        dispatch(
          setPassCount({
            day1: singing,
            day2: drama,
            day3: dance,
            redeemed_singing,
            redeemed_drama,
            redeemed_dance,
          })
        );
      } else {
      }
      dispatch(setLoading(false));
      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const { setLoading, setUserRole, setPassCount } = homeSlice.actions;

export default homeSlice.reducer;