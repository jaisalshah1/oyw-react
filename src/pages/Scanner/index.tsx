import React, { useState } from "react";
import { isMobile } from "react-device-detect";
import {
  FlexColumn,
  FlexRow,
  LoginTextInput,
  OutlinedButton,
  PageContainer,
} from "../../components/commonStyles";
import OYW21 from "../../images/icon-192x192.png";
import QRReader from "react-qr-reader";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import {
  acceptScan,
  scanPass,
  setShowQR,
  setValidateModal,
} from "../../store/passSlice";
import { ErrorText, OutfitText } from "../../components/commonText";
import Modal from "../../components/Modal";
import Close from "../../images/Close.png";
import { Navigate } from "react-router";
import cookie from "react-cookies";
import Navbar from "../../components/TicketsNavbar";

const Scanner = () => {
  const user_role = cookie.load("user_role");
  const [result, setResult] = useState<string>("");
  const [shortResult, setShortResult] = useState<string>("");
  const [day, setDay] = useState<number>(0);
  const [shortCode, setShortCode] = useState<string>("");

  const dispatch = useAppDispatch();
  const { balance, loading, validateModalOpen, showQR } = useAppSelector(
    (state) => state.entities.pass
  );
  const [selectBalance, setSelectBalance] = useState<number>(
    !loading ? balance : 0
  );
  const handleError = (err: any) => {
    alert(err);
  };
  const handleScan = (data: string) => {
    if (data !== null && data !== "" && day !== 0) {
      setResult(data);
      setSelectBalance(0);
      dispatch(scanPass({ code: data, day }));
      setSelectBalance(0);
      setShortCode("");
      dispatch(setShowQR(false));
    } else {
      if (shortCode.length >= 6 && day !== 0) {
        setShortResult(shortCode)
        dispatch(scanPass({ short_code: shortCode, day }));
        setSelectBalance(0);
        setShortCode("");
        dispatch(setShowQR(false));
      }
    }
  };

  // const [validateModalOpen, setValidateModalOpen] = useState<boolean>(false);
  if (user_role === undefined) return <Navigate to="/tickets/signin" />;

  return (
    <PageContainer>
      <Navbar />
      <FlexColumn
        style={{
          alignItems: "center",
          gap: "24px",
          padding: isMobile ? "16px" : "48px",
        }}
      >
        <img src={OYW21} alt="" style={{ height: "120px", width: "120px" }} />
        {showQR && (
          <>
            <QRReader
              delay={300}
              onError={handleError}
              onScan={(data) => handleScan(data!)}
              showViewFinder={!isMobile}
              style={{ width: isMobile ? "100%" : "480px" }}
              facingMode="environment"
            />
            <FlexRow>
              <FlexRow style={{ gap: "8px", justifyContent: "space-evenly" }}>
                <div
                  style={{
                    padding: "4px",
                    borderRadius: "4px",
                    border:
                      day === 1 ? "1px solid #182b4e" : "1px solid #369444",
                    flex: 1,
                    textAlign: "center",
                    color: day === 1 ? "#369444" : "#182b4e",
                    backgroundColor: day === 1 ? "#93f7a22f" : "white",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setDay(1);
                  }}
                >
                  Day 1
                </div>
                <div
                  style={{
                    padding: "4px",
                    borderRadius: "4px",
                    border:
                      day === 2 ? "1px solid #182b4e" : "1px solid #369444",
                    flex: 1,
                    textAlign: "center",
                    color: day === 2 ? "#369444" : "#182b4e",
                    backgroundColor: day === 2 ? "#93f7a22f" : "white",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setDay(2);
                  }}
                >
                  Day 2
                </div>
                <div
                  style={{
                    padding: "4px",
                    borderRadius: "4px",
                    border:
                      day === 3 ? "1px solid #182b4e" : "1px solid #369444",
                    flex: 1,
                    textAlign: "center",
                    color: day === 3 ? "#369444" : "#182b4e",
                    backgroundColor: day === 3 ? "#93f7a22f" : "white",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setDay(3);
                  }}
                >
                  Day 3
                </div>
              </FlexRow>
            </FlexRow>
            
            <LoginTextInput
                placeholder="ShortCode"
                onChange={(e) => setShortCode(e.target.value)}
                value={shortCode}
              />
            <p>{result}</p>
          </>
        )}
        {!showQR && (<>
          <FlexColumn style={{ alignItems: "center", gap: "16px" }}>
            {validateModalOpen && (
              <Modal width={isMobile ? "90%" : "480px"}>
                <FlexColumn style={{ gap: "16px" }}>
                  <FlexRow style={{ justifyContent: "flex-end" }}>
                    <img
                      src={Close}
                      alt=""
                      style={{ width: "24px", height: "24px" }}
                      onClick={() => dispatch(setValidateModal(false))}
                    />
                  </FlexRow>
                  <FlexColumn
                    style={{ alignItems: "center", width: "100%", gap: "16px" }}
                  >
                    <FlexColumn style={{ alignItems: "center" }}>
                      <OutfitText
                        style={{ fontWeight: "bold", fontSize: "24px" }}
                      >
                        Confirm Accept Pass
                      </OutfitText>
                      <OutfitText style={{ fontSize: "20px" }}>
                        Day {day}:<b> {selectBalance} passes left</b>
                      </OutfitText>
                    </FlexColumn>
                    <FlexRow style={{ width: "100%", gap: "16px" }}>
                      <button
                        style={{
                          width: "100%",
                          backgroundColor: "#182b4e",
                          height: "40px",
                          borderRadius: "4px",
                          color: "white",
                          cursor: "pointer",
                          fontSize: "24px",
                          marginBottom: "16px",
                          padding: "0px auto",
                        }}
                        onClick={() => dispatch(setValidateModal(false))}
                        // onClick={() => onSubmit(mobile, passcode)}
                        // onClick={() => console.log(isValid(content))}
                      >
                        <OutfitText style={{ fontSize: "20px" }}>
                          Cancel
                        </OutfitText>
                      </button>
                      {selectBalance > 0 && (
                        <button
                        style={{
                          width: "100%",
                          backgroundColor: "#182b4e",
                          height: "40px",
                          borderRadius: "4px",
                          color: "white",
                          cursor: "pointer",
                          fontSize: "24px",
                          marginBottom: "16px",
                          padding: "0px auto",
                        }}
                        // onClick={() => onSubmit(mobile, passcode)}
                        disabled={loading}
                        onClick={() => {
                          if (shortCode == ""){
                            if(selectBalance > 0){
                              dispatch(
                                acceptScan({
                                  day,
                                  count: selectBalance,
                                  code: result,
                                })
                              );
                            } else {
                                setShowQR(true);
                                setResult(""); 
                                setShortResult("");
                            }
                          } else {
                            if(selectBalance > 0){
                              dispatch(
                                acceptScan({
                                  day,
                                  count: selectBalance,
                                  short_code: shortCode,
                                })
                              );
                            } else {
                                setShowQR(true);
                                setResult("");
                                setShortResult("");
                              
                            }
                          }
                        }}
                        >
                        <OutfitText style={{ fontSize: "20px" }}>
                          Confirm
                        </OutfitText>
                        </button>
                      )}
                    </FlexRow>
                  </FlexColumn>
                </FlexColumn>
              </Modal>
            )}
            <OutfitText>Day {day} Pass Balance</OutfitText>
            <FlexRow
              style={{
                width: "100%",
                alignItems: "center",
                justifyContent: "space-between",
                gap: "16px",
              }}
            >
              <OutfitText style={{ fontSize: "24px" }}>
                {day === 1 ? "Singing" : day === 2 ? "Drama" : "Dance"}
              </OutfitText>
              <FlexColumn style={{ width: "100%", flex: 0.25 }}>
                <FlexRow
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    gap: "16px",
                  }}
                >{
                  selectBalance > 0 ? (
                    <OutfitText
                      style={{
                        borderRadius: "4px",
                        border: "1px solid #182b4e",
                        flex: 1,
                        textAlign: "center",
                        cursor: "pointer",
                        fontSize: isMobile ? "32px" : "16px",
                        width: "32px",
                        fontWeight: "bold",
                      }}
                      onClick={() => {
                        if (selectBalance > 0) {
                          setSelectBalance(selectBalance - 1);
                        }
                      }}
                    >
                      -
                    </OutfitText>
                  ) : (
                    <OutfitText
                      style={{
                        borderRadius: "4px",
                        border: "1px solid #182b4e",
                        backgroundColor: "#444444",
                        flex: 1,
                        textAlign: "center",
                        cursor: "pointer",
                        fontSize: isMobile ? "32px" : "16px",
                        width: "32px",
                        fontWeight: "bold",
                      }}
                    >
                      -
                    </OutfitText>
                  )
                }
                <OutfitText style={{ fontSize: "20px" }}>
                    {selectBalance}
                  </OutfitText>
                  {
                    selectBalance >= balance ? (
                          <OutfitText
                          style={{
                            borderRadius: "4px",
                            border: "1px solid #182b4e",
                            backgroundColor: "#444444",
                            flex: 1,
                            textAlign: "center",
                            cursor: "pointer",
                            fontSize: isMobile ? "32px" : "16px",
                            width: "32px",
                            fontWeight: "bold",
                          }}
                        >
                          +
                        </OutfitText>
                    ) : (
                        <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: isMobile ? "32px" : "16px",
                          width: "32px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (selectBalance < balance) {
                            setSelectBalance(selectBalance + 1);
                          }
                        }}
                      >
                        +
                      </OutfitText>
                    )
                  }
                </FlexRow>
              </FlexColumn>
            </FlexRow>
            
            <OutlinedButton
              style={{ border: "3px solid green" }}
              onClick={() => dispatch(setValidateModal(true))}
            >
              <OutfitText style={{ fontSize: "20px", color: "green" }}>
                Accept Pass
              </OutfitText>
            </OutlinedButton>

            <OutlinedButton
              style={{ border: "3px solid grey" }}
              onClick={() => {
                dispatch(setShowQR(true));
                setResult("");
              }}
            >
              <OutfitText style={{ fontSize: "20px", color: "grey" }}>
                Scan
              </OutfitText>
            </OutlinedButton>
          </FlexColumn>
          </>
        )}
      </FlexColumn>
    </PageContainer>
  );
};

export default Scanner;