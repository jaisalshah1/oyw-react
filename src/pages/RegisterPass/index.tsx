import { useState, useEffect } from "react";
import { isMobile } from "react-device-detect";
import {
  FlexColumn,
  FlexRow,
  LoginTextInput,
  PageContainer,
  WhiteCard,
} from "../../components/commonStyles";

import { NavHeaderText, OutfitText } from "../../components/commonText";
import { ErrorText } from "../../components/commonText";
import OYW21 from "../../images/icon-192x192.png";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { NAVBAR, PRICES } from "../../utils/constants";
import cookie from "react-cookies";
import { Navigate } from "react-router";
import {
  registerPass,
  setConfirmRegisterModalOpen,
} from "../../store/passSlice";

import Joi from "joi";
import Loader from "../../components/Loader";
import { Link } from "react-router-dom";
import MenuIcon from "@mui/icons-material/Menu";
import Navbar from "../../components/TicketsNavbar"

import Close from "../../images/Close.png";
import Modal from "../../components/Modal";

const RegisterPass = () => {
  const dispatch = useAppDispatch();
  const { message, confirmRegisterModalOpen } = useAppSelector(
    (state) => state.entities.pass
  );
  const user_role: string = cookie.load("user_role");
  const token: string = cookie.load("token");
  const { loading } = useAppSelector((state) => state.entities.home);
  const [content, setContent] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [error, setError] = useState<string>("");
  const [day1, setDay1] = useState<boolean>(false);
  const [day2, setDay2] = useState<boolean>(false);
  const [day3, setDay3] = useState<boolean>(false);
  const [count1, setCount1] = useState<number>(0);
  const [count2, setCount2] = useState<number>(0);
  const [count3, setCount3] = useState<number>(0);

  const [total, setTotal] = useState<number>(0.0);

  const RegisterPassWithMobile = Joi.object()
    .keys({
      name: Joi.string().min(3).required(),
      phone: Joi.string()
        .length(10)
        .pattern(/^[0-9]+$/)
        .required(),
      total: Joi.number()
        .min(1)
        .required()
        .error((errors: any) => {
          errors[0].message = "Please select pass details";
          return errors;
        }),
    })
    .required();

  const isValid = (content: string) => {
    if (content.length === 10 && !content.includes("@")) {
      const validate = RegisterPassWithMobile.validate({
        name,
        phone: content,
        total,
      });
      if (!validate.error) {
        dispatch(
          registerPass({
            name,
            phone: content,
            singing_count: count1,
            dance_count: count3,
            drama_count: count2,
          })
        );
      } else {
        setError(validate.error.message);
      }
      return "mobile";
    } else if (
      content.includes("@") &&
      content.includes(".") &&
      content.length > 5
    ) {
      dispatch(
        registerPass({
          name,
          email: content,
          singing_count: count1,
          drama_count: count2,
          dance_count: count3,
        })
      );
      return "email";
    }
  };

  useEffect(() => {
    setTotal(
      PRICES[1] * count1 * +!!day1 +
        PRICES[2] * count2 * +!!day2 +
        PRICES[3] * count3 * +!!day3
    );
  }, [count1, count2, count3, day1, day2, day3]);
  const [isNavOpen, setIsNavOpen] = useState<boolean>(false);

  if (user_role === undefined) return <Navigate to="/tickets/signin" />;

  return loading ? (
    <Loader />
  ) : (
    <PageContainer style={{ minHeight: "100vh", height: "100%" }}>
      <Navbar />
      {confirmRegisterModalOpen && (
        <Modal width={isMobile ? "90%" : "480px"}>
          <FlexColumn style={{ gap: "16px" }}>
            <FlexRow style={{ justifyContent: "flex-end" }}>
              <img
                src={Close}
                alt=""
                style={{ width: "24px", height: "24px" }}
                onClick={() => dispatch(setConfirmRegisterModalOpen(false))}
              />
            </FlexRow>
            <FlexColumn
              style={{ alignItems: "center", width: "100%", gap: "16px" }}
            >
              <OutfitText style={{ fontSize: "24px", fontWeight: "bold" }}>
                Are you sure?
              </OutfitText>
              <hr />
              <OutfitText style={{ fontSize: "20px" }}>
                Book passes for
              </OutfitText>
              <OutfitText style={{ fontSize: "20px", fontWeight: "bold" }}>
                {name}
              </OutfitText>
              <OutfitText style={{ fontSize: "20px", fontWeight: "bold" }}>
                {content}
              </OutfitText>
              {count1 !== 0 && (
                <FlexRow>
                  <OutfitText style={{ fontSize: "18px" }}>
                    Singing:&nbsp;
                  </OutfitText>
                  <OutfitText style={{ fontSize: "18px", fontWeight: "bold" }}>
                    {count1}
                  </OutfitText>
                </FlexRow>
              )}
              {count2 !== 0 && (
                <FlexRow>
                  <OutfitText style={{ fontSize: "18px" }}>Drama:&nbsp;</OutfitText>
                  <OutfitText style={{ fontSize: "18px", fontWeight: "bold" }}>
                    {count2}
                  </OutfitText>
                </FlexRow>
              )}
              {count3 !== 0 && (
                <FlexRow>
                  <OutfitText style={{ fontSize: "18px" }}>Dance:&nbsp;</OutfitText>
                  <OutfitText style={{ fontSize: "18px", fontWeight: "bold" }}>
                    {count3}
                  </OutfitText>
                </FlexRow>
              )}

              <FlexRow>
                <OutfitText style={{ fontSize: "20px", fontWeight: "bold" }}>
                  Amount:&nbsp;
                </OutfitText>
                <OutfitText style={{ fontSize: "20px", fontWeight: "bold" }}>
                  {total}
                </OutfitText>
              </FlexRow>

              <FlexRow style={{ width: "100%", gap: "16px" }}>
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    marginBottom: "16px",
                    padding: "0px auto",
                  }}
                  onClick={() => {
                    dispatch(setConfirmRegisterModalOpen(false));
                  }}
                >
                  <OutfitText style={{ fontSize: "20px" }}>Cancel</OutfitText>
                </button>
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    marginBottom: "16px",
                    padding: "0px auto",
                  }}
                  // onClick={() => onSubmit(mobile, passcode)}
                  disabled={loading}
                  onClick={() => {
                    console.log(isValid(content));
                    dispatch(setConfirmRegisterModalOpen(false));
                  }}
                >
                  <OutfitText style={{ fontSize: "20px" }}>Confirm</OutfitText>
                </button>
              </FlexRow>
            </FlexColumn>
          </FlexColumn>
        </Modal>
      )}
      <FlexColumn
        style={{ alignItems: "center", width: "100%", height: "100%" }}
      >
        <img src={OYW21} alt="" style={{ height: "120px", width: "120px" }} />
        <WhiteCard
          style={
            isMobile
              ? { margin: "10%", maxHeight: "50%", width: "90%" }
              : {
                  width: "400px",
                  maxWidth: "40%",
                  maxHeight: "580px",
                  padding: "36px",
                }
          }
        >
          <FlexColumn
            style={{
              gap: "32px",
              height: "100%",
            }}
          >
            <OutfitText style={{ fontSize: "32px", textAlign: "center" }}>
              Register Pass
            </OutfitText>
            <FlexColumn
              style={{
                justifyContent: "flex-end",
                height: "100%",
                gap: "16px",
              }}
            >
              <LoginTextInput
                placeholder="Mobile / Email Id"
                onChange={(e) => setContent(e.target.value)}
                value={content}
              />
              <LoginTextInput
                placeholder="Name"
                onChange={(e) => setName(e.target.value)}
                value={name}
              />

              <FlexRow
                style={{
                  width: "100%",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <FlexColumn style={{ width: "100%", flex: 0.6 }}>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: !day1 ? "1px solid #182b4e" : "1px solid #369444",
                      flex: 1,
                      width: "100%",
                      textAlign: "center",
                      color: !day1 ? "#182b4e" : "#369444",
                      backgroundColor: day1 ? "#93f7a22f" : "transparent",
                      cursor: "pointer",
                      fontWeight: 600,
                    }}
                    onClick={() => {
                      setDay1(!day1);
                      setCount1(0);
                    }}
                  >
                    <FlexRow style={{ width: "100%", flex: 0.5 }}>
                      <OutfitText style={{ fontSize: "16px" }}>
                        Singing - Day 1
                      </OutfitText>
                    </FlexRow>
                  </div>
                </FlexColumn>
                {day1 && (
                  <FlexColumn style={{ width: "100%", flex: 0.25 }}>
                    <FlexRow
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        width: "100%",
                        gap: "16px",
                      }}
                    >
                      <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: "16px",
                          maxWidth: "20px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (count1 > 1) {
                            setCount1(count1 - 1);
                          }
                        }}
                      >
                        -
                      </OutfitText>
                      <OutfitText style={{ fontSize: "20px" }}>
                        {count1}
                      </OutfitText>
                      <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: "16px",
                          maxWidth: "20px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (count1 < 4) {
                            setCount1(count1 + 1);
                          }
                        }}
                      >
                        +
                      </OutfitText>
                    </FlexRow>
                  </FlexColumn>
                )}
              </FlexRow>
              <FlexRow
                style={{
                  width: "100%",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <FlexColumn style={{ width: "100%", flex: 0.6 }}>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: !day2 ? "1px solid #182b4e" : "1px solid #369444",
                      flex: 1,
                      width: "100%",
                      textAlign: "center",
                      color: !day2 ? "#182b4e" : "#369444",
                      backgroundColor: day2 ? "#93f7a22f" : "transparent",
                      cursor: "pointer",
                      fontWeight: 600,
                    }}
                    onClick={() => {
                      setDay2(!day2);
                      setCount2(0);
                    }}
                  >
                    <FlexRow style={{ width: "100%", flex: 0.5 }}>
                      <OutfitText style={{ fontSize: "16px" }}>
                        Drama - Day 2
                      </OutfitText>
                    </FlexRow>
                  </div>
                </FlexColumn>
                {day2 && (
                  <FlexColumn style={{ width: "100%", flex: 0.25 }}>
                    <FlexRow
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        width: "100%",
                        gap: "16px",
                      }}
                    >
                      <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: "16px",
                          maxWidth: "20px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (count2 > 1) {
                            setCount2(count2 - 1);
                          }
                        }}
                      >
                        -
                      </OutfitText>
                      <OutfitText style={{ fontSize: "20px" }}>
                        {count2}
                      </OutfitText>
                      <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: "16px",
                          maxWidth: "20px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (count2 < 4) {
                            setCount2(count2 + 1);
                          }
                        }}
                      >
                        +
                      </OutfitText>
                    </FlexRow>
                  </FlexColumn>
                )}
              </FlexRow>
              <FlexRow
                style={{
                  width: "100%",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <FlexColumn style={{ width: "100%", flex: 0.6 }}>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: !day3 ? "1px solid #182b4e" : "1px solid #369444",
                      flex: 1,
                      width: "100%",
                      textAlign: "center",
                      color: !day3 ? "#182b4e" : "#369444",
                      backgroundColor: day3 ? "#93f7a22f" : "transparent",
                      cursor: "pointer",
                      fontWeight: 600,
                    }}
                    onClick={() => {
                      setDay3(!day3);
                      setCount3(0);
                    }}
                  >
                    <FlexRow style={{ width: "100%", flex: 0.5 }}>
                      <OutfitText style={{ fontSize: "16px" }}>
                        Dance - Day 3
                      </OutfitText>
                    </FlexRow>
                  </div>
                </FlexColumn>
                {day3 && (
                  <FlexColumn style={{ width: "100%", flex: 0.25 }}>
                    <FlexRow
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        width: "100%",
                        gap: "16px",
                      }}
                    >
                      <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: "16px",
                          maxWidth: "20px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (count3 > 1) {
                            setCount3(count3 - 1);
                          }
                        }}
                      >
                        -
                      </OutfitText>
                      <OutfitText style={{ fontSize: "20px" }}>
                        {count3}
                      </OutfitText>
                      <OutfitText
                        style={{
                          borderRadius: "4px",
                          border: "1px solid #182b4e",
                          flex: 1,
                          textAlign: "center",
                          cursor: "pointer",
                          fontSize: "16px",
                          maxWidth: "20px",
                          fontWeight: "bold",
                        }}
                        onClick={() => {
                          if (count3 < 4) {
                            setCount3(count3 + 1);
                          }
                        }}
                      >
                        +
                      </OutfitText>
                    </FlexRow>
                  </FlexColumn>
                )}
              </FlexRow>

              <FlexRow style={{ justifyContent: "space-between" }}>
                <OutfitText style={{ fontSize: "20px" }}>Total</OutfitText>
                <OutfitText
                  style={{
                    fontSize: "20px",
                    fontWeight: "bold",
                    marginLeft: "16px",
                  }}
                >
                  {`${total}.00`}
                </OutfitText>
              </FlexRow>
              {error && <ErrorText>{error}</ErrorText>}
              <button
                style={{
                  width: "100%",
                  backgroundColor: "#182b4e",
                  height: "40px",
                  borderRadius: "4px",
                  color: "white",
                  cursor: "pointer",
                  fontSize: "24px",
                  outlineOffset: "4px",
                  outline: "1px solid #182b4e",
                  marginBottom: "16px",
                }}
                // onClick={() => onSubmit(mobile, passcode)}
                disabled={loading}
                onClick={() => dispatch(setConfirmRegisterModalOpen(true))}
              >
                <OutfitText style={{ fontSize: "24px" }}>Submit</OutfitText>
              </button>
            </FlexColumn>
          </FlexColumn>
        </WhiteCard>

        <OutfitText style={{ fontSize: "24px", color: "green" }}>
          {message}
        </OutfitText>
      </FlexColumn>
    </PageContainer>
  );
};

export default RegisterPass;
