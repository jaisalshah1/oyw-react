import React from "react";
import { isMobile } from "react-device-detect";
import {
  FlexColumn,
  FlexRow,
  PageContainer,
} from "../../components/commonStyles";
import OYW21 from "../../images/icon-192x192.png";
import Navbar from "../../components/TicketsNavbar";
import { OutfitText } from "../../components/commonText";
import AdCarousel from "../../components/AdCarousel";
import ReactPlayer from "react-player";
import { Stream } from "@cloudflare/stream-react";
import { LIVE_ID } from "../../utils/constants";
// import { VIDEO_AD_URL } from "../../utils/constants";

const LiveStream = () => {
  return (
    <>
      <Navbar />
      <PageContainer
        style={{
          minHeight: "100vh",
          height: "175vh",
        }}
      >
        <FlexColumn
          style={{
            alignItems: "center",
            width: "100%",
            height: "100%",
            padding: "30px",
          }}
        >
          <img src={OYW21} alt="" style={{ height: "80px", width: "80px" }} />
          <OutfitText style={{ textAlign: "center" }}>
            OYW 2021 Livestream
          </OutfitText>
          <br />
          <FlexColumn
            style={{
              height: "100%",
              alignItems: "center",
              width: "100%",
              gap: "32px",
            }}
          >
            {/* <iframe
              width={isMobile ? "100%" : "100%"}
              height={isMobile ? "200px" : "75%"}
              src="https://www.youtube.com/embed/live_stream?channel=UCZE-BsJU6GCfgXgEAWtRQ6w"
              title="OYW Livestream"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            /> */}
            <iframe
              src={`https://iframe.videodelivery.net/${LIVE_ID}`}
              width={isMobile ? "100%" : "100%"}
              height={isMobile ? "200px" : "75%"}
              allow="accelerometer; gyroscope; autoplay; encrypted-media; picture-in-picture;"
              id="stream-player"
              title="OYW Livestream"
              allowFullScreen
            />

            {/* <Stream controls src={LIVE_URL} /> */}
            <FlexRow
              style={{
                width: "100%",
                justifyContent: "center",
              }}
            >
              <AdCarousel />
            </FlexRow>
            <video width="350" height="300" controls autoPlay>
              <source
                src="https://storage.googleapis.com/ads-oyw/pcpl_compressed.mp4"
                type="video/mp4"
              />
            </video>
          </FlexColumn>
        </FlexColumn>
      </PageContainer>
    </>
  );
};


export default LiveStream;
