import axios from "axios";

axios.defaults.withCredentials = true;
// axios.defaults.headers.common = {
//   "Access-Control-Allow-Origin": "*",
//   "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
// };
// axios.defaults.headers.common["Content-Type"] =
//   "application/x-www-form-urlencoded";
// axios.interceptors.response.use(
//   (response) => {
//     return response;
//   },
//   (error) => {
//     if (
//       error.response &&
//       error.response.status >= 400 &&
//       error.response.status < 500
//     ) {
//       if (error.response.status === 404) {
//         window.location.href = "/notfound";
//       } else {
//         window.location.href = "/forbidden";
//       }
//       throw error;
//     } else if (error.response && error.response.status >= 500) {
//       window.location.href = "/serverError";
//       throw error;
//     }
//   }
// );

const methods = {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
  patch: axios.patch,
};

export default methods;
